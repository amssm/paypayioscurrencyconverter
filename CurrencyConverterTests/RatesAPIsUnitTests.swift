//
//  RatesAPIsUnitTests.swift
//  CurrencyConverterTests
//
//  Created by Ahmad Mahmoud on 08/01/2021.
//

import XCTest
import RxSwift
import iOS_Bootstrap
@testable import CurrencyConverter

class RatesAPIsUnitTests: XCTestCase {
    
    private var response: Data?
    private var successDisposable: Disposable?
    private let restClient = TestingRestClient()
    
    override func tearDown() {
        self.response = nil
        self.successDisposable?.dispose()
        super.tearDown()
    }
    
    func testSuccessResponseParsingIsCorrect() throws {
        let expectation = self.expectation(description: "Execute")
        response = JsonFileLoader.getDataFromJsonFileWith(name: "CurrenciesRatesSuccessResponse")
        RestUtils.registerGetMock(endPoint: Endpoints.currencies, mockedData: response!, statusCode: 200)
        let api: Single<CurrenciesResponse> = restClient.request(api: CountriesCurrenciesAPI())
        self.successDisposable = api.subscribe({ result in
                switch result {
                case .success(let response):
                    XCTAssertNotNil(response)
                    XCTAssert(response.success)
                    XCTAssertNil(response.error)
                    XCTAssertNotNil(response.quotes)
                case .failure(let error):
                    XCTFail("did fail to parse API error response : \(error)")
                }
                expectation.fulfill()
            })
        self.waitForExpectations(timeout: 1)
    }
}

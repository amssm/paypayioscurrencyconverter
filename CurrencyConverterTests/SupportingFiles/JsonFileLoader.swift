//
//  JsonFileLoader.swift
//  CurrencyConverterTests
//
//  Created by Ahmad Mahmoud on 08/01/2021.
//

import Foundation

struct JsonFileLoader {
    
    private static func getJsonContentAsStringFromFile(fileName: String) -> String? {
        guard let file = Bundle.main.path(forResource: fileName, ofType: "json")
            else { return nil }
        return try! String(contentsOfFile: file)
    }
    
    static func getDataFromJsonFileWith(name: String) -> Data? {
        guard let jsonData = getJsonContentAsStringFromFile(fileName: name)?.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) else {
            return nil
        }
        return jsonData
    }
}

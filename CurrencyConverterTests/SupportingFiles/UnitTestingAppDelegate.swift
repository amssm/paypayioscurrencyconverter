//
//  UnitTestingAppDelegate.swift
//  CurrencyConverterTests
//
//  Created by Ahmad Mahmoud on 08/01/2021.
//

import UIKit

@objc(UnitTestingAppDelegate)
class UnitTestingAppDelegate: UIResponder, UIApplicationDelegate {
        
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return false
    }
}

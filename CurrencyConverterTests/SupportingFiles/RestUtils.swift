//
//  RestUtils.swift
//  CurrencyConverterTests
//
//  Created by Ahmad Mahmoud on 08/01/2021.
//

import Mocker
@testable import CurrencyConverter

struct RestUtils {
    
    static func registerGetMock(endPoint: String, mockedData: Data, statusCode: Int) {
        let fullURL = (EnvironmentVariables.currenciesRatesBaseURL + endPoint).toURL()
        let contentType = Mock.ContentType.json
        let mock = Mock(url: fullURL, contentType: contentType, statusCode: statusCode, data: [.get: mockedData])
        Mocker.register(mock)
    }
}

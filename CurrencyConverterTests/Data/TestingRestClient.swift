//
//  TestingRestClient.swift
//  CurrencyConverterTests
//
//  Created by Ahmad Mahmoud on 08/01/2021.
//

import Mocker
import iOS_Bootstrap
@testable import CurrencyConverter

class TestingRestClient: RxAlamofireRestClient {
    
    override func getAlamofireSessionConfiguration() -> URLSessionConfiguration {
        let configuration = super.getAlamofireSessionConfiguration()
        configuration.protocolClasses = [MockingURLProtocol.self] + (configuration.protocolClasses ?? [])
        return configuration
    }
    
    override func getBaseURL() -> String {
        return EnvironmentVariables.currenciesRatesBaseURL
    }
    
    override func shouldEnableLoadingIndicator() -> Bool {
        return false
    }
}

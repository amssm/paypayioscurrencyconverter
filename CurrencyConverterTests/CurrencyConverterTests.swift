//
//  CurrencyConverterTests.swift
//  CurrencyConverterTests
//
//  Created by Ahmad Mahmoud on 03/12/2020.
//

import XCTest
@testable import CurrencyConverter

class CurrencyConverterTests: XCTestCase {

    func testLocalCurrencyConversion() throws {
        // 1 USD to EGP
        let usdToEgpRate: Float = 15.70
        // 1 USD to AED
        let usdToAedRate: Float = 3.67
        // Expected Result
        // 1 AED to EGP
        let aedToEgpRate: Float = 4.28
        //
        let usdCurrencyInfo = CountryInfo(currencyName: "USD", currencyRate: usdToEgpRate, flag: nil)
        // AED to EGP
        let convertedCurrencyInfo = usdCurrencyInfo.copyWith(inputAmount: 1, targetConversionRate: usdToAedRate)
        // Rounded to two decimal places
        let resultRate = String(format:"%.2f", convertedCurrencyInfo.currencyRate)
        //
        assert(usdCurrencyInfo.currencyName == convertedCurrencyInfo.currencyName)
        assert(usdCurrencyInfo.currencyRate != convertedCurrencyInfo.currencyRate)
        assert(String(aedToEgpRate) == resultRate)
    }
}

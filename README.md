# PayPayiOSCurrencyConverter


1. I used MVP,but I'm also comfortable with MVVM and MVI and other design patterns.

2. I also used [iOS Bootstrap](https://github.com/ahmadmssm/iOS_Bootstrap), which is my open-source iOS library, It is a wrapper around Alamofire plus some useful utilities that facilitate the development process.

3. I also used [Resolver](https://github.com/hmlongco/Resolver) as a dependency injection framework, I'm also contributing to this open-source library.

4. I used the Navigator to handle the navigation between view controllers, Navigator is the Router layer in VIPER pattern which is responsible for the navigation from one view to another.

5. Along with the provided [Currencies rates API](https://currencylayer.com), I used this [API](https://restcountries.eu/) to fetch the world countries basically to get the countries flags, I then combined the results from the two APIs to get a new list that has both the flag and currency info.

6. Used [Mocker](https://github.com/WeTransfer/Mocker) to mock the response of the APIs to be used with unit testing.

7. Push notification is used to notify about the success of the background service.

8. There is a background task that should update the app every 30 minutes.
    If you want to test it, do the following:
    - Open this file `RefreshCurrenciesRatesBGService`and enable the breakpoint at line 55.
    - Run the app from Xcode on a physical device because this feature is not supported on the simulator.
    - When the app shows the currencies listing screen, put the app in the background, then the app should be paused and it should hit the breakpoint at line number 55.
    - Rund this command in xcode console (the right tab and not the left one)bto run the background task:
    `e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"com.ams.currencyConverterSyncronizer"]` 
    - Disable and skip the breakpoint at line number 55.
    - The task will run to update Coredata items from the remote items.
    - Once done, A notification will be pushed to indicate the success of the background task.
    - If you click on the notification item, It will reopen the app and loads the latest updates from core data.

9. All the icons in the app are from [FlatIcon](https://www.flaticon.com/), they are all free without copyright.

10. I wanted to include more unit tests but the time is very tight for me as I'm very busy with shipping a feature to about 4.7 Million users, that will help them with the COVID-19 situation.

### Screenshots :

![](https://gitlab.com/amssm/paypayioscurrencyconverter/-/raw/master/Screenshots/Notifications.png).
![](https://gitlab.com/amssm/paypayioscurrencyconverter/-/raw/master/Screenshots/Listing.png "Currencies rates list").
![](https://gitlab.com/amssm/paypayioscurrencyconverter/-/raw/master/Screenshots/Search.png "Currencies rates list with search for currency(ies)").
![](https://gitlab.com/amssm/paypayioscurrencyconverter/-/raw/master/Screenshots/Convert.png "Convert a currency to other currencies").
![](https://gitlab.com/amssm/paypayioscurrencyconverter/-/raw/master/Screenshots/Reset.png "Reset convert a currency to other currencies").

Thank you very much...


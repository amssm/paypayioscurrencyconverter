//
//  HomeListingRepo.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import RxSwift

class CurrenciesListingRepo {
    
    private let countriesInfoDB: CountriesInfoDB
    private let countriesFlagsAPI: Observable<[CountryCurrenciesAndFlag]>
    private let countriesCurrenciesAPI: Observable<CurrenciesResponse>
    
    init(countriesInfoDB: CountriesInfoDB,
         flagsAPI: Single<[CountryCurrenciesAndFlag]>,
         currenciesAPI: Single<CurrenciesResponse>) {
        self.countriesInfoDB = countriesInfoDB
        self.countriesFlagsAPI = flagsAPI.asObservable()
        self.countriesCurrenciesAPI = currenciesAPI.asObservable()
    }
    
    func getCurrenciesRatesWithFlags(forceNetwork: Bool = false) -> Single<[CountryInfo]> {
        if(forceNetwork) {
            return getRemoteCurrenciesRatesWithFlags().flatMap { remoteList in
                self.countriesInfoDB
                    .addAll(countriesInfo: remoteList)
                    .andThen(Single.just(remoteList))
            }
        }
        return countriesInfoDB.getAll().flatMap { localList in
            if(localList.isEmpty ) {
                return self.getRemoteCurrenciesRatesWithFlags().flatMap { remoteList in
                    self.countriesInfoDB
                        .addAll(countriesInfo: remoteList)
                        .andThen(Single.just(remoteList))
                    }
                }
                else {
                    return Single.just(localList.sorted { $0.currencyName < $1.currencyName })
                }
        }
    }
    
    func getRemoteCurrenciesRatesWithFlags() -> Single<[CountryInfo]> {
        var countries = [CountryInfo]()
        var countryInfo: CountryInfo? = nil
        var flag: String?
        return Observable.combineLatest(countriesFlagsAPI, getCurrenciesWithRatesObservable()) { (listWithFlags, currenciesWithRatesMap) -> [CountryInfo] in
            currenciesWithRatesMap.forEach { (key, value) in
                flag = listWithFlags.filter {$0.hasCurrencyWithSymbol(symbol: key) }.first?.flag
                countryInfo = CountryInfo(currencyName: key, currencyRate: value, flag: flag)
                countries.append(countryInfo!)
            }
            return countries.sorted { $0.currencyName < $1.currencyName }
        }.asSingle()
    }
    
    func getCurrenciesWithRatesObservable() -> Observable<[String : Float]> {
        return self.countriesCurrenciesAPI.asObservable().flatMap { result -> Observable<[String : Float]> in
            if (!result.success) {
                return Observable.error(APIError(success: false, error: result.error!))
            }
            else {
                return Observable.just(result.quotes!).map { quotes in
                    return Dictionary(uniqueKeysWithValues: quotes.map { key, value in
                        (key.replaceFirstOccurrenceOfString(of: Constants.baseCurrency, with: ""), value)
                    })
                }
           }
        }
    }
}

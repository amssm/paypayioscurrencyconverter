//
//  CurrenciesListingTableDelegate.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

protocol CurrenciesListingTableDelegate {
    func didSelect(countryInfo: CountryInfo)
}

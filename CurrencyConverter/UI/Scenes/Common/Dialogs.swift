//
//  Dialogs.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import UIKit.UIAlertController
import UIKit.UIViewController

class Dialogs {

    private var alertController: UIAlertController?

    func showDialog(viewController: UIViewController, title: String = "message".localized(), message: String) {
        alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController!.addAction(UIAlertAction(title: "ok".localized(), style: .default, handler: nil))
        viewController.present(alertController!, animated: true, completion: nil)
    }

    func showLoading(viewController: UIViewController) {
        self.alertController = UIAlertController(title: nil,
                                            message: "loading".localized(),
                                            preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        if #available(iOS 13.0, *) { loadingIndicator.style = .medium }
        else { loadingIndicator.style = .gray }
        loadingIndicator.startAnimating()
        self.alertController!.view.addSubview(loadingIndicator)
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            viewController.present(strongSelf.alertController!, animated: true, completion: nil)
        }
    }
    
    private func hideAletDialog() {
        DispatchQueue.main.async { [weak self] in
            self?.alertController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func hideLoading() {
        hideDialog()
    }

    func hideDialog() {
        hideAletDialog()
    }
}

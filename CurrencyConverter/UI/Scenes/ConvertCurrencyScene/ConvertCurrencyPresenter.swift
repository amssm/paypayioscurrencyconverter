//
//  ConvertCurrencyPresenter.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import RxSwift
import iOS_Bootstrap

class ConvertCurrencyPresenter: AppPresenter<ConvertCurrencyView> {
    
    private var countryInfo: CountryInfo!
    private var currenciesListingRepo: CurrenciesListingRepo!
    private var convertedValue: String!
    private let publishSubject = PublishSubject<String>()
    
    
    required convenience init(viewDelegate: ConvertCurrencyView,
                              countryInfo: CountryInfo,
                              currenciesListingRepo: CurrenciesListingRepo) {
        self.init(viewDelegate: viewDelegate)
        self.countryInfo = countryInfo
        self.currenciesListingRepo = currenciesListingRepo
    }
    
    override func viewControllerDidLoad() {
        self.observeUserInput()
    }
    
    func convertCurrency(amount: String) {
        self.publishSubject.on(.next(amount))
    }
    
    private func observeUserInput() {
        self.publishSubject
            .map { Float($0) ?? 0.0 }
            .debounce(RxTimeInterval.milliseconds(500), scheduler: RxSchedulers.main)
            .flatMap { self.getConvertedRatesListObservable(inputAmount: $0) }
            .subscribe(on: RxSchedulers.backgroundConcurrentScheduler)
            .observe(on: RxSchedulers.main)
            .subscribe { [weak self] event in
                switch (event) {
                case .next(let listOfConvertedRates):
                    self?.getViewDelegate().didGetConvertedRates(rates: listOfConvertedRates)
                case .error(let error):
                    self?.getViewDelegate().didGetError(errorMessage: error.localizedDescription)
                default: break
                }
            }
            .disposed(by: disposeBag)
    }
    
    private func doLocalConversion(amount: Float) -> Float {
        return amount * countryInfo.currencyRate
    }

    private func getConvertedRatesListObservable(inputAmount: Float) -> Observable<[CountryInfo]> {
        return currenciesListingRepo
            .getCurrenciesRatesWithFlags()
            .asObservable()
            .map {
                // Remove current currency from the list
                $0.filter {
                    $0.currencyName != self.countryInfo.currencyName
                }.map {
                    // Convert each currency to the selected currency rate
                    // taking into consideration the input amount
                    $0.copyWith(inputAmount: inputAmount, targetConversionRate: self.countryInfo.currencyRate)
                }
            }
    }
}

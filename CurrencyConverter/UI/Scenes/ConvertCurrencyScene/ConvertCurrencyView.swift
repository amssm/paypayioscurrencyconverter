//
//  ConvertCurrencyView.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

protocol ConvertCurrencyView: AppViewDelegate {
    func didGetConvertedRates(rates: [CountryInfo]) 
}

//
//  ConvertCurrencyViewController.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import UIKit

class ConvertCurrencyViewController: AppViewController<ConvertCurrencyPresenter, ConvertCurrencyView>,
                                     ConvertCurrencyView {

    @IBOutlet private weak var targetCurrencyLabel: UILabel!
    @IBOutlet private weak var targetCurrencyTextField: UITextField!
    @IBOutlet private weak var ratesTableView: UITableView!
    //
    private var countryInfo: CountryInfo!
    private lazy var tableViewAdapter = {
        CurrenciesListingTableAdapter(tableView: self.ratesTableView, currenciesListingTableDelegate: nil)
    }()
    
    convenience init(countryInfo: CountryInfo) {
        self.init()
        self.countryInfo = countryInfo
    }
    
    override func initPresenter () -> ConvertCurrencyPresenter? {
        return resolver.optional(arguments: self, countryInfo!)
    }
    
    override func initUI() {
        self.targetCurrencyLabel.text = countryInfo.currencyName
        self.targetCurrencyTextField.placeholder = "write_down_you_amount".localized()
        // Remove top bar from keyboard
        self.targetCurrencyTextField.inputAccessoryView = UIView()
    }
    
    func didGetConvertedRates(rates: [CountryInfo]) {
        self.tableViewAdapter.reloadSinglePageTable(items: rates)
        self.ratesTableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
    }
    
    @IBAction private  func textFieldDidChange(_ sender: UITextField) {
        self.getPresenter().convertCurrency(amount: sender.text ?? "0.0")
    }
}

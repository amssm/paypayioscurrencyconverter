//
//  CurrenciesListingViewController.xib.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import iOS_Bootstrap

class CurrenciesListingViewController: AppViewController<CurrenciesListingPresenter, CurrenciesListingView>,
                                       CurrenciesListingView,
                                       CurrenciesListingTableDelegate,
                                       UISearchBarDelegate {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var baseCurrencyFlagImageView: UIImageView!
    @IBOutlet private weak var baseCurrencyLabel: UILabel!
    @IBOutlet private weak var searchBar: UISearchBar!
    //
    private lazy var tableViewAdapter = {
        CurrenciesListingTableAdapter(tableView: self.tableView, currenciesListingTableDelegate: self)
    }()
    
    override func initPresenter() -> CurrenciesListingPresenter {
        return resolver.resolve(args: self)
    }
    
    override func initUI() {
        self.view.backgroundColor = .blueColor
        self.baseCurrencyLabel.text = "base_currency".localized() + " : " + Constants.baseCurrency
        self.configureSearchBar()
    }
    
    func didGet(currenciesRatesWithFlags: [CountryInfo]) {
        self.tableViewAdapter.reloadSinglePageTable(items: currenciesRatesWithFlags)
    }
    
    func didSelect(countryInfo: CountryInfo) {
        self.navigator.openConvertCurrencyViewController(from: self, countryInfo: countryInfo)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.getPresenter().didUpdateUserInput(text: searchText)
    }
    
    private func configureSearchBar() {
        self.searchBar.delegate = self
        self.searchBar.tintColor = .blueColor
        self.searchBar.barTintColor = .blueColor
        // Hide srach bar cancel button
        self.searchBar.searchTextField.backgroundColor = .white
        self.searchBar.searchTextField.layer.cornerRadius = 15
        self.searchBar.searchTextField.layer.masksToBounds = true
        self.searchBar.searchTextField.textColor = .black
        self.searchBar.searchTextField.tintColor = .black
        self.searchBar.searchTextField.placeholder = "search_for_a_currency".localized()
        self.searchBar.layer.borderWidth = 1
        self.searchBar.layer.borderColor = UIColor.blueColor.cgColor
    }
}

//
//  CurrenciesListingView.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import iOS_Bootstrap

protocol CurrenciesListingView: AppViewDelegate {
    func didGet(currenciesRatesWithFlags: [CountryInfo])
}

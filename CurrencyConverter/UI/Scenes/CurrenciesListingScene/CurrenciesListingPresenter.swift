//
//  CurrenciesListingPresenter.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import RxSwift
import iOS_Bootstrap

class CurrenciesListingPresenter: AppPresenter<CurrenciesListingView> {
    
    private var currenciesListingRepo: CurrenciesListingRepo!
    private var listOfCountriesWithInfo: [CountryInfo] = []
    private let searchPublishSubject = PublishSubject<String>()
    
    required convenience init(viewDelegate: CurrenciesListingView, currenciesListingRepo: CurrenciesListingRepo) {
        self.init(viewDelegate: viewDelegate)
        self.currenciesListingRepo = currenciesListingRepo
    }
    
    override func viewControllerDidLoad() {
        self.listenForSearchText()
        self.fetchCurrenciesRatesWithFlags()
    }
    
    override func viewControllerWillRefresh() {
        // Observe and reload if the App is opened from psuh notifications.
        let selector = #selector(self.fetchCurrenciesRatesWithFlags)
        NotificationCenterUtils.observeDefaultPushNotificationAction(observer: self, selector: selector)
    }
    
    func didUpdateUserInput(text: String) {
        self.searchPublishSubject.onNext(text)
    }
    
    @objc private func fetchCurrenciesRatesWithFlags() {
        self.getViewDelegate().showLoading()
        self.currenciesListingRepo
            .getCurrenciesRatesWithFlags()
            .applyThreadingConfig()
            .subscribe(onSuccess: { [weak self] currenciesRatesWithFlags in
                self?.getViewDelegate().hideLoading()
                self?.listOfCountriesWithInfo = currenciesRatesWithFlags
                self?.getViewDelegate().didGet(currenciesRatesWithFlags: currenciesRatesWithFlags)
            }, onFailure: { [weak self] error in
                self?.postError(errorMessage: error.localizedDescription)
            })
            .disposed(by: disposeBag)
    }
    
    private func listenForSearchText() {
        self.searchPublishSubject
            // Skip 1 because I want to skip the initial.
            .skip(1)
            .debounce(RxTimeInterval.milliseconds(500), scheduler: RxSchedulers.main)
            .flatMap { text -> Observable<[CountryInfo]> in
                if (text.isEmpty) {
                    return Observable.just(self.listOfCountriesWithInfo)
                }
                else {
                    return Observable.just(self.listOfCountriesWithInfo.filter { $0.currencyName.contains(text.uppercased())
                    })
                }
            }
            .subscribe(on: RxSchedulers.backgroundConcurrentScheduler)
            .observe(on: RxSchedulers.main)
            .subscribe { [weak self] event in
                switch (event) {
                case .next(let currenciesRatesWithFlags):
                    self?.getViewDelegate().didGet(currenciesRatesWithFlags: currenciesRatesWithFlags)
                case .error(let error):
                    self?.getViewDelegate().didGetError(errorMessage: error.localizedDescription)
                default: break
                }
            }
            .disposed(by: disposeBag)
        }
    
    override func viewControllerWillDisappear() {
        NotificationCenterUtils.removeObserver(observer: self)
    }
}

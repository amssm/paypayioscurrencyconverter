//
//  AppDelegate.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import UIKit
import Resolver
import iOS_Bootstrap

class AppDelegate: UIResponder, UIApplicationDelegate {

    private var appDelegateServices: [UIApplicationDelegate] = {
        let defaultConfigs: AppDefaultConfigurationsService = Resolver.resolve()
        let fetchingService: RefreshCurrenciesRatesBGService = Resolver.resolve()
        let navigator: Navigator = Resolver.resolve()
        return [defaultConfigs, fetchingService, navigator]
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Reset notification badge.
        UIApplication.shared.applicationIconBadgeNumber = 0
        self.appDelegateServices.forEach {
            _ = $0.application?(application, didFinishLaunchingWithOptions: launchOptions)
        }
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        self.appDelegateServices.forEach { $0.applicationDidEnterBackground?(application) }
    }
}

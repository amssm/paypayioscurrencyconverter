//
//  PushNotificationsUtils.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 06/01/2021.
//

import UIKit
import UserNotifications

class PushNotificationsUtils: NSObject, UNUserNotificationCenterDelegate {
    
    private let userNotificationCenter = UNUserNotificationCenter.current()

    override init() {
        super.init()
        self.userNotificationCenter.delegate = self
    }
    
    func requestNotificationAuthorization(errorAction: @escaping (Error?) -> Void) {
        let authOptions = UNAuthorizationOptions.init(arrayLiteral: .alert, .badge, .sound)
        self.userNotificationCenter.requestAuthorization(options: authOptions) { (success, error) in
            if let error = error {
                errorAction(error)
            }
        }
    }
    
    func sendNotification() {
        self.userNotificationCenter.getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                let content = UNMutableNotificationContent()
                content.title = "currencirs_rates_title".localized()
                content.body = "currencirs_rates_message".localized()
                content.sound = UNNotificationSound.default
                content.badge = 1
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                let identifier = EnvironmentVariables.pushNotificationId
                let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                self.userNotificationCenter.add(request) { (error) in
                    if let error = error {
                        print("Error \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if(response.actionIdentifier == Notification.Name.defaultPushNotificationAction.rawValue) {
            NotificationCenterUtils.postDefaultPushNotificationAction()
        }
        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}

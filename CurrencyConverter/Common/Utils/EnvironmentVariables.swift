//
//  EnvironmentVariables.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 07/01/2021.
//

import Foundation

struct EnvironmentVariables {
    
    private static var bundle = { Bundle.main }()
   
    static var pushNotificationId = {
        getStringWith(key: "PushNotificationId")
    }()

    static var backgroundSyncServiceId = {
        getStringWith(key: "BackgroundSyncServiceId")
    }()

    static var currenciesRatesAPIKey = {
        getStringWith(key: "CurrenciesRatesAPIKey")
    }()

    static var currenciesRatesBaseURL = {
        getStringWith(key: "CurrenciesRatesBaseURL")
    }()

    static var countriesFlagsBaseURL = {
        getStringWith(key: "CountriesFlagsBaseURL")
    }()
    
    static var coreDataContainerlName = {
        getStringWith(key: "CoreDataContainerlName")
    }()

    private static func getStringWith(key: String) -> String {
        self.bundle.object(forInfoDictionaryKey: key) as! String
    }
}

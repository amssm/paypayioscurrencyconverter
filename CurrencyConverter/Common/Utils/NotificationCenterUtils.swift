//
//  NotificationCenterUtils.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 08/01/2021.
//

import Foundation

struct NotificationCenterUtils {
    
    static func postDefaultPushNotificationAction() {
        NotificationCenter.default.post(name: Notification.Name.defaultPushNotificationAction, object: nil)
    }
    
    static func observeDefaultPushNotificationAction(observer: Any, selector: Selector) {
        let notificationName = Notification.Name.defaultPushNotificationAction
        NotificationCenter.default.addObserver(observer, selector: selector, name: notificationName, object: nil)
    }
    
    static func removeObserver(observer: Any) {
        NotificationCenter.default.removeObserver(observer)
    }
}

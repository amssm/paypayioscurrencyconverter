//
//  AppDefaultConfigurationsService.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 06/01/2021.
//

import iOS_Bootstrap

class AppDefaultConfigurationsService: AppDelegateService {
    
    private let defaultConfigs: DefaultConfigurations
    
    init(defaultConfigs: DefaultConfigurations) {
        self.defaultConfigs = defaultConfigs
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        self.initEssentialAppConfigurations()
        return true
    }
    
    private func initEssentialAppConfigurations() {
        let navigationBarTextStyle = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white]
        _ = defaultConfigs
            .enableIQKeyboard()
            .iqKeyboardCanGoBack()
            .iqKeyboardCanGoNext()
            .setNavigationBarTextApperance(textApperance: navigationBarTextStyle)
            .setNavigationBarBackButtonColor(backButtonColor: UIColor.white)
    }
}

//
//  Navigator.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import UIKit
import iOS_Bootstrap

class Navigator: AppDelegateService {
    
    private var window: UIWindow!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.startInitialViewController()
        return true
    }
    
    
    private func startInitialViewController() {
        let mainViewController = CurrenciesListingViewController()
        UIView.transition(
            with: window,
            duration: 0.3,
            options: .transitionFlipFromLeft,
            animations: {
                self.window.rootViewController = mainViewController
                self.window.makeKeyAndVisible()
        }, completion: nil)
    }
    
    func openConvertCurrencyViewController(from viewController: UIViewController, countryInfo: CountryInfo)  {
        let destinationViewController = ConvertCurrencyViewController(countryInfo: countryInfo)
        viewController.present(destinationViewController, animated: true, completion: nil)
    }
}

//
//  RefreshCurrenciesRatesBackgroundService.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 05/01/2021.
//

import RxSwift
import iOS_Bootstrap
import BackgroundTasks

class RefreshCurrenciesRatesBGService: AppDelegateService, UNUserNotificationCenterDelegate {
    
    private let taskId = EnvironmentVariables.backgroundSyncServiceId
    private let currenciesListingRepo: CurrenciesListingRepo
    private let pushNotificationsUtils: PushNotificationsUtils
    private var currenciesRatesWithFlagsDisposable: Disposable?
    
    init(notificationsUtil: PushNotificationsUtils, currenciesListingRepo: CurrenciesListingRepo) {
        self.currenciesListingRepo = currenciesListingRepo
        self.pushNotificationsUtils = notificationsUtil
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        self.registerBackgroundTask()
        self.registerLocalNotification()
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        self.cancelAllPendingBGTasks()
        self.scheduleCurrencyRatesLocalDataRefresh()
    }
    
    private func registerBackgroundTask() {
        let queue = DispatchQueue.global()
        BGTaskScheduler.shared.register(forTaskWithIdentifier: taskId, using: queue) { [weak self] (task) in
            print("BackgroundAppRefreshTaskScheduler is executed!")
            print("Background time remaining: \(UIApplication.shared.backgroundTimeRemaining)s")
            self?.currenciesRatesWithFlagsDisposable?.dispose()
            task.expirationHandler = {
                self?.currenciesRatesWithFlagsDisposable?.dispose()
                task.setTaskCompleted(success: false)
            }
            self?.refreshCurrenciesRatesWithFlags(task: task as! BGAppRefreshTask)
        }
    }
    
    func scheduleCurrencyRatesLocalDataRefresh() {
        let refreshTask = BGAppRefreshTaskRequest(identifier: taskId)
        // Refresh after 30 minutes.
        refreshTask.earliestBeginDate = Date(timeIntervalSinceNow: 30 * 60)
        do {
            try BGTaskScheduler.shared.submit(refreshTask)
            print("Submitted task request")
        }
        catch {
            print("Unable to submit task: \(error.localizedDescription)")
        }
    }
    
    private func cancelAllPendingBGTasks() {
        self.currenciesRatesWithFlagsDisposable?.dispose()
        BGTaskScheduler.shared.cancelAllTaskRequests()
    }
    
    private func refreshCurrenciesRatesWithFlags(task: BGAppRefreshTask)  {
        _ = self.currenciesListingRepo.getCurrenciesRatesWithFlags(forceNetwork: true).asCompletable()
            .subscribe(onCompleted: { [weak self] in
                self?.pushNotificationsUtils.sendNotification()
                task.setTaskCompleted(success: true)
            }, onError: { error in
                print("Error updating currency data : \(error.localizedDescription)")
                task.setTaskCompleted(success: false)
           })
    }
    
    private func registerLocalNotification() {
        pushNotificationsUtils.requestNotificationAuthorization(errorAction: { _ in
            print("User has declined notifications")
        })
    }
}

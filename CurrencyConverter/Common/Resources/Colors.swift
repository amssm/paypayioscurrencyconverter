//
//  Colors.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import UIKit

extension UIColor {
    static let blueColor = UIColor.color(fromHexString: "#03A9F4")
}

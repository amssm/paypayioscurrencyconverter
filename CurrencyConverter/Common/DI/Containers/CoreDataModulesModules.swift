//
//  CoreDataModulesModules.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import Resolver

extension Resolver {
    static func coreDataModulesModules() {
        register { CountriesInfoDB(withName: EnvironmentVariables.coreDataContainerlName) }
    }
}

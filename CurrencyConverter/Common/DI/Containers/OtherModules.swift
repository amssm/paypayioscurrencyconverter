//
//  OtherModules.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import Resolver
import iOS_Bootstrap

extension Resolver {
    static func otherModules() {
        register { Dialogs() }
        register { DefaultConfigurations() }
        register { PushNotificationsUtils() }
        register { Navigator() }.scope(application)
        register { SVGImageLoader() }.scope(application)
        register { NSCache<NSString, UIImage>() }.scope(application)
        register { AppDefaultConfigurationsService(defaultConfigs: resolve()) }
        register {
            RefreshCurrenciesRatesBGService(notificationsUtil: resolve(), currenciesListingRepo: resolve())
        }
    }
}

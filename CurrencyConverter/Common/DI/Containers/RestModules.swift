//
//  RestModules.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import RxSwift
import Resolver
import iOS_Bootstrap

extension Resolver {

    static func restClientModule() {
        register { AppRestClient() }.implements(RxAlamofireClientProtocol.self).scope(application)
    }

    static func restAPIsModules() {
        register(Single<[CountryCurrenciesAndFlag]>.self) { getRxAPI(api: WorldCountriesAPI()) }
        register(Single<CurrenciesResponse>.self) { getRxAPI(api: CountriesCurrenciesAPI()) }
    }
}


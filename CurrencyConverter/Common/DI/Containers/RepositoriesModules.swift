//
//  RepositoriesModules.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import Resolver

extension Resolver {
    static func repositoriesModules() {
        register {
            CurrenciesListingRepo(countriesInfoDB: resolve(), flagsAPI: resolve(), currenciesAPI: resolve())
        }.scope(application)
    }
}

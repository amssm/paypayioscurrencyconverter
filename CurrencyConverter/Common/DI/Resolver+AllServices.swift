//
//  Resolver+AllServices.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import Resolver

extension Resolver: ResolverRegistering {
    
    static func setResolverDefaultScope() {
        Resolver.defaultScope = Resolver.unique
    }
    
    public static func registerAllServices() {
        setResolverDefaultScope()
        presenterModules()
        restClientModule()
        restAPIsModules()
        coreDataModulesModules()
        repositoriesModules()
        otherModules()
    }
}

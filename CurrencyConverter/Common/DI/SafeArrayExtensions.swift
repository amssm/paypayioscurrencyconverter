//
//  SafeArrayExtensions.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

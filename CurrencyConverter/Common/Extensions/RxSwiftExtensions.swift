//
//  RxSwiftExtensions.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import RxSwift
import iOS_Bootstrap

extension PrimitiveSequence {
    func applyThreadingConfig() -> PrimitiveSequence {
        return self
            .subscribe(on: RxSchedulers.backgroundConcurrentScheduler)
            .observe(on: RxSchedulers.main)
    }
}

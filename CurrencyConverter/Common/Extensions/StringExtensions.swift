//
//  StringExtensions.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 03/01/2021.
//

extension String {
    func replaceFirstOccurrenceOfString(of target: String, with replaceString: String) -> String {
        guard let range = self.range(of: target) else { return self }
        return self.replacingCharacters(in: range, with: replaceString)
    }
}

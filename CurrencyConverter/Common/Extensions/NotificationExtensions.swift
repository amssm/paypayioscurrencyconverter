//
//  NotificationExtensions.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 08/01/2021.
//

import Foundation

extension Notification.Name {
    static var defaultPushNotificationAction: Notification.Name {
        return .init("com.apple.UNNotificationDefaultActionIdentifier")
    }
}

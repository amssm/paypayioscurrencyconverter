//
//  CountryInfo.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

struct CountryInfo: Decodable, Equatable {
    
    let currencyName: String
    var currencyRate: Float
    var flag: String?
    
    init(currencyName: String, currencyRate: Float, flag: String?) {
        self.currencyName = currencyName
        self.currencyRate = currencyRate
        self.flag = flag
    }
    
    func copyWith(inputAmount: Float, targetConversionRate: Float) -> CountryInfo {
        let newRate = (inputAmount/targetConversionRate) * currencyRate
        return CountryInfo(currencyName: currencyName, currencyRate: newRate, flag: flag)
    }
}

//
//  CurrenciesResponse.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

struct CurrenciesResponse: Decodable {
    let success: Bool
    var error: CurrencyLayerAPIErrorDetails?
    var quotes: [String : Float]?
}

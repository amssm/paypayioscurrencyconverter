//
//  CountryCurrenciesAndFlag.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

struct CountryCurrenciesAndFlag: Decodable {
    
    var flag: String?
    var currencies: [Currency] = []
    
    func hasCurrencyWithSymbol(symbol: String) -> Bool {
        return currencies.firstIndex(where: { $0.code == symbol }) != nil
    }
}

//
//  Currency.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

struct Currency: Decodable {
    var name: String?
    var code: String?
}

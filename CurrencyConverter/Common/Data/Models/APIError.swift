//
//  APIError.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

struct APIError: Error {
    
    let success: Bool
    let error: CurrencyLayerAPIErrorDetails
    
    init(success: Bool, error: CurrencyLayerAPIErrorDetails) {
        self.success = success
        self.error = error
    }
    
    func getErrorCode() -> Int {
        return error.code
    }

    func getErrorMessage() -> String {
        return error.info
    }
}

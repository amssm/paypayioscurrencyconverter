//
//  CurrencyLayerAPIErrorDetails.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

struct CurrencyLayerAPIErrorDetails: Decodable {
    let type: String
    let info: String
    let code: Int
}

//
//  AppRestClient.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import RxSwift
import iOS_Bootstrap

class AppRestClient: RxAlamofireRestClient {
    
    override func getBaseURL() -> String {
        return EnvironmentVariables.currenciesRatesBaseURL
    }
    
    override func getAdditionalRequestIntercepters() -> [RequestIntercepterProtocol] {
        return [BaseURLChanger(), AdditionalQueryParams()]
    }
    
    override func shouldEnableLoadingIndicator() -> Bool {
        return false
    }
}

//
//  Endpoints.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

enum Endpoints {
    static let flags = "all?fields=currencies;flag"
    static let currencies = "live"
}

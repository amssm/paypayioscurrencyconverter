//
//  AdditionalQueryParams.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 04/01/2021.
//

import iOS_Bootstrap

class AdditionalQueryParams: RequestIntercepterProtocol {
    
    func getUpdatedURLRequest(_ urlRequest: URLRequest, for session: Session) -> URLRequest {
        let url = urlRequest.url
        var request = urlRequest
        if let urlString = url?.absoluteString {
            let baseURL = EnvironmentVariables.currenciesRatesBaseURL
            let domain = baseURL
                .replacingOccurrences(of: "http:", with: "")
                .replacingOccurrences(of: "/", with: "")
            if (urlString.contains(domain)) {
                var urlComponents = URLComponents(url: url!, resolvingAgainstBaseURL: false)
                let dict = ["access_key" : EnvironmentVariables.currenciesRatesAPIKey]
                let queryItems = dict.map { return URLQueryItem(name: "\($0)", value: "\($1)") }
                urlComponents?.queryItems = queryItems
                request.url = urlComponents?.url
            }
        }
        return request
    }
}

//
//  CountriesInfoDB.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 03/01/2021.
//

import RxSwift

class CountriesInfoDB: CoreDataManager<CountryInfoCoreDataEntity> {
    
    final func addAll(countriesInfo: [CountryInfo]) -> Completable {
        let countriesEntites = countriesInfo.map { countryInfo -> CountryInfoCoreDataEntity in
            let entity = createNewEntity()
            entity.currencyName = countryInfo.currencyName
            entity.currencyRate = countryInfo.currencyRate
            entity.flag = countryInfo.flag
            return entity
        }
        return self.insertRecordsCompletable(records: countriesEntites)
    }
    
    final func getAll() -> Single<[CountryInfo]> {
        return self.fetchAll().map { entites -> [CountryInfo] in
            return entites.filter { $0.currencyName != nil }.map { entity -> CountryInfo in
                return CountryInfo(currencyName: entity.currencyName!,
                                   currencyRate: entity.currencyRate,
                                   flag: entity.flag)
            }
        }
    }
}

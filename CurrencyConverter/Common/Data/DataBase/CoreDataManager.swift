//
//  CoreDataManager.swift
//  CurrencyConverter
//
//  Created by Ahmad Mahmoud on 03/01/2021.
//

import Foundation
import iOS_Bootstrap
import CoreData
import RxSwift

class CoreDataManager<T: NSManagedObject> {
    
    private let coreDataContainerName: String
    private lazy var context: NSManagedObjectContext! = {
        let container = NSPersistentContainer(name: coreDataContainerName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {
                fatalError("Unresolved error, \((error as NSError).userInfo)")
            }
        })
        let context = container.viewContext
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return context
    }()
    
    init(withName name: String) {
        self.coreDataContainerName = name
    }
    
    final func insertRecord(record: T) -> Bool {
        context.insert(record)
        do {
            try context.save()
            return true
        }
        catch _ {
            return false
        }
    }

    final func insertRecords(records : [T]) -> Bool {
        var success = true
        records.forEach { record in
            if(insertRecord(record: record) == false) {
                success = false
            }
        }
        return success
    }
    
    final func insertRecordsCompletable(records : [T]) -> Completable {
        return Completable.create { completable in
            if(self.insertRecords(records: records)) {
                completable(.completed)
            }
            else {
                completable(.error(CustomErrorBuilder.initWith("Error inserting data to core data", 1991)))
            }
            return Disposables.create {}
        }
    }
    
    final func fetchAll() -> Single<[T]> {
        return Single.create { single in
            var items : [T] = []
            do {
                let arrayOfItems = try self.context.fetch(self.getFetchReuest()) as! [T]
                arrayOfItems.forEach { item in items.append(item) }
                single(.success(arrayOfItems))
            }
            catch let error { single(.failure(error)) }
            return Disposables.create()
        }
    }    

    final func getFetchReuest() -> NSFetchRequest<NSFetchRequestResult> {
        let entityName: String = T.classForCoder().description()
        return NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
    }
    
    final func createNewEntity() -> T {
        return T.init(context: self.context)
    }
}
